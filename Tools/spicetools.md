>[!info] About
>Adjusted build of [spicetools](https://github.com/spicetools/spicetools) with better wine integration

>[!warning]
>See this [third party guide](https://two-torial.xyz/games/iidx31/setup/#configuring-spice2x) if you were looking for more generic spicetools usage

